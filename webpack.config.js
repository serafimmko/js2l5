module.exports = {
  entry: './review',
  output: {
    path: __dirname,
    filename: 'build.js',
  },

  watch: true,

  watchOptions: {
    aggregateTimeout: 100,
  },
};
